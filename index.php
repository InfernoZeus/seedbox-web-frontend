<!DOCTYPE html>
<html>
<head>
  <title>Select Episode</title>
  <style type="text/css">
    * {
      margin:0;
      padding:0;
    }

    body {
      background: url("images/fabric_of_squares_gray.png");
    }

    a {
      /*text-decoration: none;*/
      color: #888;
      font-weight: bold;
    }

    #form {
      width: 200px;
      position: relative;
      left: 50%;
      margin-left: -100px;
    }

    .select {
        float: left;
        width: 200px;
        padding: 10px;
        vertical-align: middle;
        font-size: 15px;
    }

    .recent_select {
        float: left;
        width: 200px;
        padding: 10px;
        vertical-align: middle;
        font-size: 15px;
    }

    .row {
      width: 200px;
      padding: 10px 0px;
      height: 40px;
      text-align: center;
    }

    .top-row {
      padding-top: 0px;
    }

    .bottom-row {
      padding-bottom: 0px;
    }

    .recent-row {
      padding-bottom: 70px;
      padding-top: 60px;
    }

    .row.checkbox {
    }

    .row.checkbox label {
      position: relative;
      top: 40%;
    }

    .row a {
      position: relative;
      top: 30%;
    }

    input[type=checkbox] {
      margin-right: 4px;
      position: relative;
      bottom: 2px;
    }

    .watchButton {
      padding: 7px;
      width: 90px;
      font-size: 15px;
      margin: 5px;
    }

    .hidden {
      display: none;
    }

    #clear {
        clear: both;
    }
  </style>
  <link href="tabs.css" rel="stylesheet" type="text/css">
</head>
<body>
  <?php


  $mtime = microtime();
  $mtime = explode(" ",$mtime);
  $mtime = $mtime[1] + $mtime[0];
  $starttime = $mtime; 

  include 'watched.php';

  try {
    $WATCHED_KEY="_WATCHED_";

    $recentdata = array();
    $data = array();

    $files = getAllFiles();

    foreach($files as $entry) {
      if(is_dir($dir_path.$entry)) {
# FIX THIS
      } else {
        $file = $entry;
        if (endsWith($file, ".mp4")) {

          $current_user = $_SERVER["PHP_AUTH_USER"];
          $watched = checkWatchedState($current_user, $entry);
          $modified_time = filemtime($dir_path . '/' . $entry);
          $recentdata_entry = array($entry, $watched, $modified_time);
          $recentdata[] = $recentdata_entry;


            // Convert 00x00 format to S00E00 format
          if (preg_match("/.*[\.\s](([0-9]{1,2})x([0-9]{1,2}))[\.\s].*/", $file, $matches)) {
            $file = str_replace($matches[2]."x".$matches[3], "S".$matches[2]."E".$matches[3], $file);
          }
            // Check the filename matches ABC.S00E00.ABC.mp4 format
          if (preg_match("/(.*)[\.\s]([sS][0-9]{1,}([eE][0-9]{1,}){1,2}(-[eE][0-9]{1,})?)[\.\s](.*)\.mp4/", $file, $matches)) {

              // Tidy up show name
            $show = $matches[1];
            $replace = array(".", "_", "-");
            $show = str_replace($replace, " ", $show);
            $show = trim($show);

              // Tidy up episode ID
            $epId = $matches[2];
            $epId = str_replace("-", "", $epId);

            // Tidy up release version/info
            $releaseInfo = $matches[5];

              // Extract Season and Episode numbers
            preg_match("/[sS]([0-9]{1,})[eE]([0-9]{1,})(?:[eE]([0-9]{1,}))?/", $epId, $epIdMatches);
            $season = $epIdMatches[1];
            if (count($epIdMatches) == 3) {
              $episode = $epIdMatches[2];
            } else if (count($epIdMatches) == 4) {
              $episode = $epIdMatches[2].$epIdMatches[3];
            }

            if (isset($data[$show])) {
              $show_array = $data[$show];
            } else {
              $show_array = array();
            }

            if (isset($show_array[$season])) {
              $season_array = $show_array[$season];
            } else {
              $season_array = array();
            }

            if (isset($season_array[$episode])) {
              $episode_array = $season_array[$episode];
            } else {
              $episode_array = array();
            }

            $episode_array[$releaseInfo] = $entry;
            $episode_array[$WATCHED_KEY] = $watched;

            $season_array[$episode] = $episode_array;
            $show_array[$season] = $season_array;
            $data[$show] = $show_array;
          }
        } else {
          echo "Not MP4: ".$file;
        }
      }
    }

    usort($recentdata, 'sortByModifiedDate');

  } catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
  }




  $mtime = microtime();
  $mtime = explode(" ",$mtime);
  $mtime = $mtime[1] + $mtime[0];
  $endtime = $mtime;
  $totaltime = ($endtime - $starttime);
  echo "This page was created in ".$totaltime." seconds";


  ?>

<input type="hidden" id="refreshed" value="no">

<div id="tabContainer">
    <div class="tabs">
      <ul>
        <li id="tabHeader_1" style="margin-right: 2px;">Select Episode</li>
        <li id="tabHeader_2" style="margin-left: 2px;">Recent Episodes</li>
      </ul>
    </div>
    <div class="tabscontent">
      <div class="tabpage" id="tabpage_1">
        <form id="form" action="player.html" onsubmit="return validateForm();">
          <div class="row top-row"><select class="select" id="show" disabled="true" onChange="updateSeasons(this);"></select><div id="clear"></div></div>
          <div class="row"><select class="select" id="season" disabled="true" onChange="updateEpisodes(this);"></select><div id="clear"></div></div>
          <div class="row"><select class="select" id="episode" disabled="true" onChange="updateFiles(this);"></select><div id="clear"></div></div>
          <div class="row hidden" id="hiddenFileSelect"><select class="select" id="file" disabled="true" onChange="updateButton();"></select><div id="clear"></div></div>
          <div class="row bottom-row"><input type="submit" class="watchButton" id="watchButton" value="Watch" disabled="true"/><input type="button" class="watchButton" id="downloadButton" value="Download" disabled="true" onClick="downloadFile();"/></div>
          <input type="hidden" id="filename" name="filename" value="">
        </form>
      </div>
      <div class="tabpage" id="tabpage_2">
        <form id="form" action="player.html" onsubmit="return validateRecentForm();">
          <div class="row top-row checkbox"><label><input type="checkbox" id="hideWatchedCheckbox" onChange="updateRecent();" />Hide Watched Episodes</label></div>
          <div class="row"><select class="recent_select" id="recent_select" disabled="true" onChange="updateRecentButton();"></select><div id="clear"></div></div>
          <div class="row"><a href="manage.php">Manage Watched Episodes</a></div>
          <div class="row bottom-row"><input type="submit" class="watchButton" id="watchRecentButton" value="Watch" disabled="true"/><input type="button" class="watchButton" id="downloadRecentButton" value="Download" disabled="true" onClick="downloadRecentFile();"/></div>
          <input type="hidden" id="filenameRecent" name="filename" value="">
        </form>
      </div>
    </div>
  </div>



  <script type="text/javascript">
  "use strict";

  var data = <? echo json_encode($data); ?>;
  var recentdata = <? echo json_encode($recentdata); ?>;
  var WATCHED_KEY = <? echo json_encode($WATCHED_KEY); ?>;
  var WATCHED_ERROR = <? echo json_encode($WATCHED_ERROR); ?>;

  function numericSort(array) {
    return array.sort(function(a,b){return a-b});
  }

  function resetSelect(select, disable, addExtra, text) {
    select.options.length = 0;
    if (addExtra) {
      var option = document.createElement("option");
      option.value = "";
      option.text = text;
      select.add(option);
    }
    if (disable) {
      select.disabled=true;
    }
  }

  function setFileSelectHidden(state, reset) {
    reset = typeof reset !== 'undefined' ? reset : true;
    var files = document.getElementById("hiddenFileSelect");
    var file_select = document.getElementById("file");
    if (state) {
      files.className = 'row hidden';
    } else {
      files.className = 'row';
    }
    if (reset) {
      resetSelect(file_select, true, true, "Select a Version...");
    }
  }

  function updateShows() {
    var shows = Object.keys(data).sort();
    var shows_select = document.getElementById("show");
    var season_select = document.getElementById("season");
    var episode_select = document.getElementById("episode");
    var watch_button = document.getElementById("watchButton");

    //Reset and add blank option to all three
    resetSelect(shows_select, true, true, "Select a Show...");
    resetSelect(season_select, true, true, "Select a Season...");
    resetSelect(episode_select, true, true, "Select an Episode...");
    setFileSelectHidden(true);
    watchButton.disabled=true;

    for (var i = 0; i < shows.length; i++) {
      var option = document.createElement("option");
      option.value = option.text = shows[i];
      shows_select.add(option);
    }
    shows_select.disabled=false;
  }

  function updateSeasons(source) {
    var shows_select = document.getElementById("show");
    var season_select = document.getElementById("season");
    var episode_select = document.getElementById("episode");
    var watch_button = document.getElementById("watchButton");

    //Reset and add blank option
    resetSelect(season_select, true, true, "Select a Season...");
    resetSelect(episode_select, true, true, "Select an Episode...");
    setFileSelectHidden(true);
    watchButton.disabled=true;

    var show = shows_select.value;
    if (show === "") {
      return;
    }
    var seasons = numericSort(Object.keys(data[show]));
    for (var i = 0; i < seasons.length; i++) {
      var option = document.createElement("option");
      option.value = option.text = seasons[i];
      season_select.add(option);
    }
    season_select.disabled=false;
  }

  function updateEpisodes(source) {
    var shows_select = document.getElementById("show");
    var season_select = document.getElementById("season");
    var episode_select = document.getElementById("episode");
    var watch_button = document.getElementById("watchButton");

    //Reset and add blank option
    resetSelect(episode_select, true, true, "Select an Episode...");
    setFileSelectHidden(true);
    watchButton.disabled=true;
    
    var show = shows_select.value;
    var season = season_select.value;
    if (season === "") {
      return;
    }
    var episodes = numericSort(Object.keys(data[show][season]));
    for (var i = 0; i < episodes.length; i++) {
      var option = document.createElement("option");
      option.value = option.text = episodes[i];
      episode_select.add(option);
    }
    episode_select.disabled=false;
  }

  function updateFiles(source) {
    var shows_select = document.getElementById("show");
    var season_select = document.getElementById("season");
    var episode_select = document.getElementById("episode");
    var file_select = document.getElementById("file");
    var watch_button = document.getElementById("watchButton");

    setFileSelectHidden(true, true);
    watchButton.disabled=true;
    
    var show = shows_select.value;
    var season = season_select.value;
    var episode = episode_select.value;
    if (episode === "") {
      return;
    }
    var files = Object.keys(data[show][season][episode]);
    for (var i = 0; i < files.length; i++) {
      var text = files[i];
      if (text === WATCHED_KEY)
        continue;
      var option = document.createElement("option");
      option.value = option.text = text;
      file_select.add(option);
    }
    if (file_select.length > 2) {
      setFileSelectHidden(false, false);
      file_select.disabled=false;
    }
    updateButton();
    
  }

  function updateButton() {
    var watch_button = document.getElementById("watchButton");
    var download_button = document.getElementById("downloadButton");
    var file_select = document.getElementById("file");
    if (file_select.value === "") {
      watch_button.disabled=true;
      download_button.disabled=true;
    } else {
      watch_button.disabled=false;
      download_button.disabled=false;
    }

    var show = document.getElementById("show").value;
    var season = document.getElementById("season").value;
    var episode = document.getElementById("episode").value;
    var file = document.getElementById("file").value;
    var filename = document.getElementById("filename");

    var fullFilename = data[show][season][episode][file];
    filename.value = fullFilename;

  }

  function updateRecent() {
    var recent_select = document.getElementById("recent_select");
    var hideWatchedCheckbox = document.getElementById("hideWatchedCheckbox");
    var watch_button = document.getElementById("watchRecentButton");

    var hideWatched = hideWatchedCheckbox.checked;

    console.log("updateRecent called");

    //Reset and add blank option
    resetSelect(recent_select, true, true, "Select a File...");
    watch_button.disabled=true;

    for (var key in recentdata) {
      if (recentdata.hasOwnProperty(key)) {
        var entry = recentdata[key];
        if (hideWatched && entry[1] === true) {
          continue;
        }
        var option = document.createElement("option");
        option.value = option.text = entry[0];
        recent_select.add(option);
      }
    }

    if (recent_select.length == 1) {
      resetSelect(recent_select, true, true, "All watched.");
    } else {
      recent_select.disabled=false;
    }
  }

  function updateRecentButton() {
    var recent_select = document.getElementById("recent_select");
    var watch_button = document.getElementById("watchRecentButton");
    var download_button = document.getElementById("downloadRecentButton");
    if (recent_select.value === "") {
      watch_button.disabled=true;
      download_button.disabled=true;
    } else {
      watch_button.disabled=false;
      download_button.disabled=false;
    }
    var filename = document.getElementById("filenameRecent");
    filename.value = recent_select.value;
  }

  function validateForm() {
    var filename = document.getElementById("filename").value;
    if (filename === "") {
      return false;
    }
  }

  function validateRecentForm() {
    var filename = document.getElementById("filenameRecent").value;
    if (filename === "") {
      return false;
    }
  }

  function downloadFile() {
    var filename = document.getElementById("filename").value;
    if (filename === "") {
      return;
    }
    window.open("ajax.php?action=download&filename=" + filename);
  }

  function downloadRecentFile() {
    var filename = document.getElementById("filenameRecent").value;
    if (filename === "") {
      return;
    }
    window.open("ajax.php?action=download&filename=" + filename);
  }

  function refreshIfNeeded() {
    var e=document.getElementById("refreshed");
    if(e.value=="no")
      e.value="yes";
    else{
      e.value="no";
      window.location.reload(true); 
    }
  }
  </script>
  <script src="tabs.js"></script>
  <script type="text/javascript">
    window.onload = function() {
      refreshIfNeeded();
      updateShows();
      updateRecent();
      loadTabs();
    };
  </script>

</body>
</html>
