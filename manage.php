<!DOCTYPE html>
<html>
<head>
  <title>Manage Episodes</title>
  <style type="text/css">
    * {
      margin:0;
      padding:0;
    }

    body {
      background: url("images/fabric_of_squares_gray.png");
    }

    a {
      /*text-decoration: none;*/
      color: #888;
      font-weight: bold;
    }

    #wrapper {
      width: 800px;
      position: relative;
      left: 50%;
      margin-left: -400px;
    }

    #headers{ 
      border-bottom: 1px solid #B5B5B5;
      height: 20px;
    }

    .header {
      float: left;
      padding: 0 10px;
      vertical-align: middle;
      font-size: 15px;
    }

    #seen-header {
      width: 30px;  
    }

    #title-header {
      width: 660px;  
    }

    #date-header {
      width: 50px;  
    }

    .row {
      border-bottom: 1px solid #B5B5B5;
      height: 27px;
    }
    .row.highlight {
      background-color: #EFEFEF;
    }

    .checkbox {
      float: left;
      width: 30px;
      vertical-align: middle;
      font-size: 15px;
      padding: 5px 10px;
    }

    .title {
      float: left;
      width: 660px;
      vertical-align: middle;
      font-size: 15px;
      padding: 5px 10px;
    }

    .date {
      float: left;
      width: 50px;
      vertical-align: middle;
      font-size: 15px;
      padding: 5px 10px;
    }

    input[type=checkbox] {
      margin-left:  8px;
      position: relative;
      top: 1px;
    }

    #clear {
        clear: both;
    }
  </style>
</head>

<body>
  <div id="wrapper">
    <div><a href="./"><div style="height: 30px; float: left;"><img src="images/back.png" style="height: 16px; padding-top: 5px; float: left;"/><span style="float: left; padding-top: 4px; padding-left: 5px;">Back</span></div></a><div style="float: right; padding-top: 8px; font-size: 12px;">Click on headers to sort.</div></div><div id="clear"></div>
    <div id="headers"><div class="header" id="seen-header"><a href="#" onClick="sort('seen')">Seen</a></div><div class="header" id="title-header"><a href="#" onClick="sort('title')">Title</a></div><div class="header" id="date-header"><a href="#" onClick="sort('date')">Date</a></div></div><div id="clear"></div>
<?php

  include 'watched.php';
  $current_user = $_SERVER["PHP_AUTH_USER"];
  $allFiles = getAllFiles();
  $files = array();
  foreach($allFiles as $entry) {
    if(!is_dir($dir_path.$entry)) {
      if (endsWith($entry, ".mp4")) {
        $modified_time = filemtime($dir_path . '/' . $entry);
        $watched = checkWatchedState($current_user, $entry);
        $files[] = array($entry, $watched, $modified_time);
      }
    }
  }

  if(isset($_REQUEST['sort']) && !empty($_REQUEST['sort']) && isset($_REQUEST['order']) && !empty($_REQUEST['order'])) {
    $sort = $_REQUEST['sort'];
    $order = $_REQUEST['order'];
    if ($sort === "seen") {
      if ($order === "asc") {
        usort($files, 'sortBySeen');
      } elseif ($order === "dsc"){
        usort($files, 'sortBySeenReverse');
      } else {
        usort($files, 'sortByModifiedDate');
      }
    } elseif ($sort === "title") {
      if ($order === "asc") {
        usort($files, 'sortByTitle');
      } elseif ($order === "dsc"){
        usort($files, 'sortByTitleReverse');
      } else {
        usort($files, 'sortByModifiedDate');
      }
    } elseif ($sort === "date") {
      if ($order === "asc") {
        usort($files, 'sortByModifiedDate');
      } elseif ($order === "dsc"){
        usort($files, 'sortByModifiedDateReverse');
      } else {
        usort($files, 'sortByModifiedDate');
      }
    } else {
      usort($files, 'sortByModifiedDate');
    }
  } else {
    usort($files, 'sortByModifiedDate');
  }

  foreach($files as $entry) {
    echo '<div class="row"><label><div class="checkbox" onMouseOver="highlightRow(this);" onMouseOut="unhighlightRow(this);"><input type="checkbox" onChange="updateWatchedState(this);" value="';
    echo $entry[0];
    echo '" ';
    if ($entry[1])
      echo 'checked ';
    echo '/></div><div class="title" onMouseOver="highlightRow(this);" onMouseOut="unhighlightRow(this);">';
    echo $entry[0];
    echo '</div><div class="date" onMouseOver="highlightRow(this);" onMouseOut="unhighlightRow(this);">';
    echo date("d/m/y", $entry[2]);
    echo '</div></label></div><div id="clear"></div>';
    echo "\n";
  }

?>
  </div>

  <script type="text/javascript">

  function highlightRow(trigger) {
    var parent = trigger.parentElement;
    while (parent.className !== "row") {
      if (parent.parentElement == null)
        break;
      parent = parent.parentElement;
    }
    if (parent == null)
      return;
    parent.className = "row highlight";
  }

  function unhighlightRow(trigger) {
    var parent = trigger.parentElement;
    while (parent.className !== "row highlight") {
      if (parent.parentElement == null)
        break;
      parent = parent.parentElement;
    }
    if (parent == null)
      return;
    parent.className = "row";
  }

  function updateWatchedState(checkbox) {
    var filename = checkbox.value;
    var newState = checkbox.checked;
    var xmlHttp = null;
    var url = "ajax.php?action=watch&state=" + newState + "&filename=" + filename;

    xmlHttp = new XMLHttpRequest(); 
    // xmlHttp.onreadystatechange = reactToWatchedState;
    xmlHttp.open("POST", url);
    xmlHttp.send();
  }

  function sort(sort) {
    var vars = getUrlVars();
    if (vars['order'] === null || vars['sort'] === null || sort !== vars['sort']) {
      if (sort === "date")
        order="dsc";
      else
        order="asc";
    }
    else {
      if (vars['order'] === "asc")
        order="dsc";
      else
        order="asc";
    }
    window.location.href="manage.php?sort="+sort+"&order="+order;
  }

  function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = decodeURIComponent(value);
    });
    return vars;
  }

  </script>
</body>
</html>