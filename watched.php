<?php 

$dir_path = "../torrents/showrss/";
$watched_folder_path = "./watched/";

function checkWatchedState($user, $filename) {
  
  global $watched_folder_path;

  if (empty($user)) {
    return false;
  }

  $watched_file_path = $watched_folder_path.$user;
  if (!is_readable($watched_file_path)) {
    return false;
  }

  $watched_file = file_get_contents($watched_file_path);
  if ($watched_file === false) {
    return false;
  }

  $pos = strpos($watched_file, $filename);
  if ($pos !== false) {
    return true;
  } else {
    return false;
  }
}

function markWatchedState($user, $filename, $newState) {

  global $watched_folder_path;

  if (empty($user)) {
    return false;
  }
    
  $watched_file_path = $watched_folder_path.$user;

  $watched_file = file_get_contents($watched_file_path);
  if ($watched_file === false) {
    return false;
  }

  $pos = strpos($watched_file, $filename);
  if ($pos === false) {
    $curr_state = false;
  } else {
    $curr_state = true;
  }

  $line = $filename . "\n";
  if ($curr_state === false && $newState === true) {
    if (file_put_contents($watched_file_path, $line, FILE_APPEND | LOCK_EX) !== false) {
      $curr_state = true;
    }
  } elseif ($curr_state === true && $newState === false) {
    $watched_file = str_replace($line, '', $watched_file);
    if (file_put_contents($watched_file_path, $watched_file, LOCK_EX) !== false) {
      $curr_state = false;
    }
  }
  return $curr_state;
}

function markAllWatched($user) {
  $files = getAllFiles();
  foreach($files as $entry) {
    if(is_dir($dir_path.$entry)) {
  # FIX THIS
    } else {
      if (endsWith($entry, ".mp4")) {
        markWatchedState($user, $entry, true);
      }
    }
  }
}

function getAllFiles() {
  global $dir_path;
  $exclude_list = array(".", "..", ".htaccess");
  return array_diff(scandir($dir_path), $exclude_list);
}

function endsWith($string, $test) {
  $strlen = strlen($string);
  $testlen = strlen($test);
  if ($testlen > $strlen) return false;
  return substr_compare($string, $test, -$testlen) === 0;
}

function sortBySeenReverse($a, $b) {
  return sortBySeen($a, $b, -1);
}

function sortByTitleReverse($a, $b) {
  return sortByTitle($a, $b, -1);
}

function sortByModifiedDateReverse($a, $b) {
  return sortByModifiedDate($a, $b, -1);
}

function sortBySeen($a, $b, $order = 1) {
  if ($a[1] === $b[1]) {
    return sortByTitle($a, $b, 1);
  } elseif ($a[1])
    return $order*1;
  else
    return $order*-1;
}

function sortByTitle($a, $b, $order = 1) {
  return $order * strcmp($a[0], $b[0]);
}

function sortByModifiedDate($a, $b, $order = 1) {
  $diff = $b[2] - $a[2];
  if ($diff < 0)
    return -1*$order;
  if ($diff > 0)
    return 1*$order;
  return 0;
}

?>  